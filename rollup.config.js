import babel from "rollup-plugin-babel";
import commonjs from "rollup-plugin-commonjs";
import resolve from "rollup-plugin-node-resolve";
import replace from "rollup-plugin-replace";

import { eslint } from "rollup-plugin-eslint";
import { uglify } from "rollup-plugin-uglify";


export default { 
    input: "src/main.js",
    output: {
        file: "dist/bundle.js",
        format: "iife"
    },
    plugins: [
        replace({
            "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV || "development")
        }),
        resolve({
            browser: true,
            extensions: [ ".js", ".jsx", ".json" ],
        }),
        commonjs({
            include: "node_modules/**",
            exclude: [
                'node_modules/process-es6/**'
            ],
            namedExports: {
                // 'node_modules/react/index.js': ['Children', 'Component', 'PropTypes', 'createElement'],
                "node_modules/react-dom/index.js": ["render"]
            }
        }),
        babel({
            exclude: "node_modules/**",
            configFile: "./babel.config.js",
        }),
        eslint({
            configFile: "./eslint.config.js",
            exclude: [ "node_modules/**", "dist/bundle.js", "dist/index.html" ]
        }),
        process.env.NODE_ENV || "development" === "production" && uglify(),
    ]
}
