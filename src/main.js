import React from "react"; // eslint-disable-line
import { render } from "react-dom";

import HelloWorld from "./components/HelloWorld";


const app = document.querySelector("#app");
render(<HelloWorld />, app); // Render the application entrypoint
